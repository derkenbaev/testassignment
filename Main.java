import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class Main {
	public static void main(String[] args) throws Exception{
		String user = args[0];
		String repo = args[1];
		
		PullRequestsPage prs = new PullRequestsPage(user, repo);
		
		int cnt = 0;
		for (int i = 0; i < prs.getSize(); i++) {
			PullRequest a = prs.getPullRequestAt(i);
			if (a.isOpen()) {
				openNewTabInBrowser(a.getURL());
				cnt++;
			}
		}
		if (cnt == 0) {
			System.out.println("There are no open pull requests in repository " + user + "/" + repo);
		}
	}
	
	static public void openNewTabInBrowser (String url) throws IOException, URISyntaxException {
		Desktop.getDesktop().browse(new URI(url));
	}
	
	static public String[] parseJSONToStringArray(String s) {
		ArrayList<String> values = new ArrayList<String> ();
		int currentBeginOfValue = 0;
		
		int levelOfCurlyBraces = 0;
		int levelOfQuotation = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '\"' && (i == 0 || s.charAt(i - 1) != '\\')) {
				levelOfQuotation = levelOfQuotation == 0 ? 1 : 0;
			}
			else if (s.charAt(i) == '{' && levelOfQuotation == 0) {
				levelOfCurlyBraces++;
			}
			else if (s.charAt(i) == '}' && levelOfQuotation == 0) {
				levelOfCurlyBraces--;
			}
			
			else if (s.charAt(i) == ',' && levelOfCurlyBraces == 0 && levelOfQuotation == 0) {
				values.add(s.substring(currentBeginOfValue, i));
				currentBeginOfValue = i + 2;
				
			}
		}
		if (s.length() > 0) {
			values.add(s.substring(currentBeginOfValue));
		}
		
		String[] v = new String[values.size()];
		for (int i = 0; i < v.length; i++) {
			v[i] = values.get(i);
		}
		return v;
	}
	
}
