import java.util.HashMap;
import java.util.Map;

public class PullRequest {
	Map <String, String> map;
	Links links;
	public PullRequest(String s){
		map = new HashMap<String, String>();
		String[] values = Main.parseJSONToStringArray(s.substring(1, s.length() - 1));
		
		for (int i = 0; i < values.length; i++) {
			addValueToMap(values[i]);
		}
		
		links = new Links(map.get("links"));
		
		
	}
	
	
	public boolean isOpen() {
		return map.get("state").equals("\"OPEN\"");
	}
	
	public String getURL (){
		return links.getURLOfPullRequest();
	}
	
	private void addValueToMap(String s){
		
		int divideIndex = s.indexOf(':');
		String key = s.substring(1, divideIndex - 1), 
				value = s.substring(divideIndex + 2);
		map.put(key, value);		
	}
	
}

class Links {
	Map<String, String> links;
	
	public Links(String s){
		links = new HashMap<String, String>();
		String[] values = Main.parseJSONToStringArray(s.substring(1, s.length() - 1));
		
		for (int i = 0; i < values.length; i++) {
			addLinkToMap(values[i]);
		}
		
	}
	
	private void addLinkToMap(String s) {
		int divideIndex = s.indexOf(':');
		String key = s.substring(1, divideIndex - 1), 
				value = parseURL(s.substring(divideIndex + 1));
		links.put(key, value);
	}

	private String parseURL(String s) {
		return s.substring(s.indexOf(':') + 3, s.lastIndexOf('\"'));
	}
	
	public String getURLOfPullRequest() {
		return links.get("html");
	}
	
}