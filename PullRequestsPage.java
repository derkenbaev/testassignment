import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.bind.DatatypeConverter;
import java.util.Map;
import java.util.Scanner;

public class PullRequestsPage {
	
	private int pagelen;
	private PullRequest[] PRs;
	private int page;
	private int size;
	
	
	public PullRequestsPage (String user, String repo) throws Exception {
		URL url = new URL("https://api.bitbucket.org/2.0/repositories/" + user + "/" + repo + "/pullrequests");
		String input = "";
		
		try {
			Scanner scan = new Scanner(url.openStream());
			input = scan.nextLine();
			scan.close();
		} catch (Exception e) {
			String respond = e.getMessage();
			if (respond.indexOf("403") != -1) {
				System.out.println("This is a private repostory");
				System.out.print("Password: ");
				Scanner cmdScanner = new Scanner(System.in);
				String password = cmdScanner.nextLine();
				cmdScanner.close();
			
				String au = DatatypeConverter.printBase64Binary((user + ":" + password).getBytes());
				URLConnection urlconnection = url.openConnection();
				urlconnection.setRequestProperty("Authorization", "Basic " + au);
				
				char[] cha = new char[10000];
				try {
					InputStreamReader isr = new InputStreamReader( urlconnection.getInputStream());
					isr.read(cha);
					input = new String(cha);
					input = input.substring(0, input.lastIndexOf("}") + 1);
				} catch (Exception ex) {
					throw new Exception("Incorrect password");
				}
				
			}
			else {
				throw new Exception("There is no repository " + user + "/" + repo);
			}
			
		}	
		
		int beginOfLastValue = input.lastIndexOf(':') + 2;
		int endOfLastValue = input.length() - 1;
		while (input.charAt(endOfLastValue) == '}') {
			endOfLastValue--;
		}
		if (input.charAt(beginOfLastValue) == '\"') {
			throw new Exception(input.substring(beginOfLastValue + 1, endOfLastValue));
		}
		
		size = Integer.parseInt(input.substring(beginOfLastValue, endOfLastValue + 1));
		
		String[] values = Main.parseJSONToStringArray(input.substring(input.indexOf('[') + 1, input.lastIndexOf(']')));
		
		
		PRs = new PullRequest[size];
		
		for (int i = 0; i < size; i++) {
			PRs[i] = new PullRequest(values[i]);
		}
		
		
	}
	
	public int getSize() {
		return size;
	}
	
	public PullRequest getPullRequestAt(int index){
		return PRs[index];
	}
	
	
	
}
