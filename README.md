# TestAssignment

This command line tool takes username and repository's name and open all open pull requests in browser. Example command for running:

	java Main google narrowhg

If the repository is private, program asks for a password.
If there is no such repository or incorrect password program will throw an exception.
If there are not open pull requests, program will inform.
Program was written in the way that it is possible to work with rest of data but URL easier.